


#include "pitches.h"
// change this to make the song slower or faster
int tempo = 90;

// change this to whichever pin you want to use
int buzzer = 11;

// boutons 
int buttonApin = 9;
int buttonBpin = 8;
int buttonCpin = 7;
// notes of the moledy followed by the duration.
// a 4 means a quarter note, 8 an eighteenth , 16 sixteenth, so on
// !!negative numbers are used to represent dotted notes,
// so -4 means a dotted quarter note, that is, a quarter plus an eighteenth!!
int melody1[] = {

  // Game of Thrones
  // Score available at https://musescore.com/user/8407786/scores/2156716

  NOTE_G4,8, NOTE_C4,8, NOTE_DS4,16, NOTE_F4,16, NOTE_G4,8, NOTE_C4,8, NOTE_DS4,16, NOTE_F4,16, //1
  NOTE_G4,8, NOTE_C4,8, NOTE_DS4,16, NOTE_F4,16, NOTE_G4,8, NOTE_C4,8, NOTE_DS4,16, NOTE_F4,16,
  NOTE_G4,8, NOTE_C4,8, NOTE_E4,16, NOTE_F4,16, NOTE_G4,8, NOTE_C4,8, NOTE_E4,16, NOTE_F4,16,
  NOTE_G4,8, NOTE_C4,8, NOTE_E4,16, NOTE_F4,16, NOTE_G4,8, NOTE_C4,8, NOTE_E4,16, NOTE_F4,16,
  NOTE_G4,-4, NOTE_C4,-4,//5

  NOTE_DS4,16, NOTE_F4,16, NOTE_G4,4, NOTE_C4,4, NOTE_DS4,16, NOTE_F4,16, //6
  NOTE_D4,-1, //7 and 8
  NOTE_F4,-4, NOTE_AS3,-4,
  NOTE_DS4,16, NOTE_D4,16, NOTE_F4,4, NOTE_AS3,-4,
  NOTE_DS4,16, NOTE_D4,16, NOTE_C4,-1, //11 and 12

  //repeats from 5
  NOTE_G4,-4, NOTE_C4,-4,//5

  NOTE_DS4,16, NOTE_F4,16, NOTE_G4,4, NOTE_C4,4, NOTE_DS4,16, NOTE_F4,16, //6
  NOTE_D4,-1, //7 and 8
  NOTE_F4,-4, NOTE_AS3,-4,
  NOTE_DS4,16, NOTE_D4,16, NOTE_F4,4, NOTE_AS3,-4,
  NOTE_DS4,16, NOTE_D4,16, NOTE_C4,-1, //11 and 12
  NOTE_G4,-4, NOTE_C4,-4,
  NOTE_DS4,16, NOTE_F4,16, NOTE_G4,4,  NOTE_C4,4, NOTE_DS4,16, NOTE_F4,16,

  NOTE_D4,-2,//15
  NOTE_F4,-4, NOTE_AS3,-4,
  NOTE_D4,-8, NOTE_DS4,-8, NOTE_D4,-8, NOTE_AS3,-8,
  NOTE_C4,-1,
  NOTE_C5,-2,
  NOTE_AS4,-2,
  NOTE_C4,-2,
  NOTE_G4,-2,
  NOTE_DS4,-2,
  NOTE_DS4,-4, NOTE_F4,-4, 
  NOTE_G4,-1,
  
  NOTE_C5,-2,//28
  NOTE_AS4,-2,
  NOTE_C4,-2,
  NOTE_G4,-2, 
  NOTE_DS4,-2,
  NOTE_DS4,-4, NOTE_D4,-4,
  NOTE_C5,8, NOTE_G4,8, NOTE_GS4,16, NOTE_AS4,16, NOTE_C5,8, NOTE_G4,8, NOTE_GS4,16, NOTE_AS4,16,
  NOTE_C5,8, NOTE_G4,8, NOTE_GS4,16, NOTE_AS4,16, NOTE_C5,8, NOTE_G4,8, NOTE_GS4,16, NOTE_AS4,16,
  
  REST,4, NOTE_GS5,16, NOTE_AS5,16, NOTE_C6,8, NOTE_G5,8, 
};

int melody2[] = {
      NOTE_AS4, 8 , NOTE_AS4, 8 , NOTE_AS4, 8 , // 1
  NOTE_F5, 2 , NOTE_C6, 2 ,
  NOTE_AS5, 8 , NOTE_A5, 8 , NOTE_G5, 8 , NOTE_F6, 2 , NOTE_C6, 4 ,  
  NOTE_AS5, 8 , NOTE_A5, 8 , NOTE_G5, 8 , NOTE_F6, 2 , NOTE_C6, 4 ,  
  NOTE_AS5, 8 , NOTE_A5, 8 , NOTE_AS5, 8 , NOTE_G5, 2 , NOTE_C5, 8 , NOTE_C5, 8 , NOTE_C5, 8 ,
  NOTE_F5, 2 , NOTE_C6, 2 ,
  NOTE_AS5, 8 , NOTE_A5, 8 , NOTE_G5, 8 , NOTE_F6, 2 , NOTE_C6, 4 ,  
  
  NOTE_AS5, 8 , NOTE_A5, 8 , NOTE_G5, 8 , NOTE_F6, 2 , NOTE_C6, 4 , // 8  
  NOTE_AS5, 8 , NOTE_A5, 8 , NOTE_AS5, 8 , NOTE_G5, 2 , NOTE_C5, - 8 , NOTE_C5, 16 ,
  NOTE_D5, - 4 , NOTE_D5, 8 , NOTE_AS5, 8 , NOTE_A5, 8 , NOTE_G5, 8 , NOTE_F5, 8 ,
  NOTE_F5, 8 , NOTE_G5, 8 , NOTE_A5, 8 , NOTE_G5, 4 , NOTE_D5, 8 , NOTE_E5, 4 , NOTE_C5, - 8 , NOTE_C5, 16 ,
  NOTE_D5, - 4 , NOTE_D5, 8 , NOTE_AS5, 8 , NOTE_A5, 8 , NOTE_G5, 8 , NOTE_F5, 8 ,
  
  NOTE_C6, - 8 , NOTE_G5, 16 , NOTE_G5, 2 , REST, 8 , NOTE_C5, 8 , // 13
  NOTE_D5, - 4 , NOTE_D5, 8 , NOTE_AS5, 8 , NOTE_A5, 8 , NOTE_G5, 8 , NOTE_F5, 8 ,
  NOTE_F5, 8 , NOTE_G5, 8 , NOTE_A5, 8 , NOTE_G5, 4 , NOTE_D5, 8 , NOTE_E5, 4 , NOTE_C6, - 8 , NOTE_C6, 16 ,
  NOTE_F6, 4 , NOTE_DS6, 8 , NOTE_CS6, 4 , NOTE_C6, 8 , NOTE_AS5, 4 , NOTE_GS5, 8 , NOTE_G5, 4 , NOTE_F5, 8 ,
  NOTE_C6, 1
};

int melody3[] = {
   NOTE_E5, 16 , NOTE_DS5, 16 , // 1
  NOTE_E5, 16 , NOTE_DS5, 16 , NOTE_E5, 16 , NOTE_B4, 16 , NOTE_D5, 16 , NOTE_C5, 16 ,
  NOTE_A4, - 8 , NOTE_C4, 16 , NOTE_E4, 16 , NOTE_A4, 16 ,
  NOTE_B4, - 8 , NOTE_E4, 16 , NOTE_GS4, 16 , NOTE_B4, 16 ,
  NOTE_C5, 8 , REST, 16 , NOTE_E4, 16 , NOTE_E5, 16 , NOTE_DS5, 16 ,
  
  NOTE_E5, 16 , NOTE_DS5, 16 , NOTE_E5, 16 , NOTE_B4, 16 , NOTE_D5, 16 , NOTE_C5, 16 , // 6
  NOTE_A4, - 8 , NOTE_C4, 16 , NOTE_E4, 16 , NOTE_A4, 16 ,
  NOTE_B4, - 8 , NOTE_E4, 16 , NOTE_C5, 16 , NOTE_B4, 16 ,
  NOTE_A4, 4 , REST, 8 , // 9 - 1ère fin

  // répétitions de 1 se terminant le 10
  NOTE_E5, 16 , NOTE_DS5, 16 , // 1
  NOTE_E5, 16 , NOTE_DS5, 16 , NOTE_E5, 16 , NOTE_B4, 16 , NOTE_D5, 16 , NOTE_C5, 16 ,
  NOTE_A4, - 8 , NOTE_C4, 16 , NOTE_E4, 16 , NOTE_A4, 16 ,
  NOTE_B4, - 8 , NOTE_E4, 16 , NOTE_GS4, 16 , NOTE_B4, 16 ,
  NOTE_C5, 8 , REST, 16 , NOTE_E4, 16 , NOTE_E5, 16 , NOTE_DS5, 16 ,
  
  NOTE_E5, 16 , NOTE_DS5, 16 , NOTE_E5, 16 , NOTE_B4, 16 , NOTE_D5, 16 , NOTE_C5, 16 , // 6
  NOTE_A4, - 8 , NOTE_C4, 16 , NOTE_E4, 16 , NOTE_A4, 16 ,
  NOTE_B4, - 8 , NOTE_E4, 16 , NOTE_C5, 16 , NOTE_B4, 16 ,
  NOTE_A4, 8 , REST, 16 , NOTE_B4, 16 , NOTE_C5, 16 , NOTE_D5, 16 , // 10 - 2ème fin
  // continue de 11
  NOTE_E5, - 8 , NOTE_G4, 16 , NOTE_F5, 16 , NOTE_E5, 16 ,
  NOTE_D5, - 8 , NOTE_F4, 16 , NOTE_E5, 16 , NOTE_D5, 16 , // 12
  
  NOTE_C5, - 8 , NOTE_E4, 16 , NOTE_D5, 16 , NOTE_C5, 16 , // 13
  NOTE_B4, 8 , REST, 16 , NOTE_E4, 16 , NOTE_E5, 16 , REST, 16 ,
  REST, 16 , NOTE_E5, 16 , NOTE_E6, 16 , REST, 16 , REST, 16 , NOTE_DS5, 16 ,
  NOTE_E5, 16 , REST, 16 , REST, 16 , NOTE_DS5, 16 , NOTE_E5, 16 , NOTE_DS5, 16 ,
  NOTE_E5, 16 , NOTE_DS5, 16 , NOTE_E5, 16 , NOTE_B4, 16 , NOTE_D5, 16 , NOTE_C5, 16 ,
  NOTE_A4, 8 , REST, 16 , NOTE_C4, 16 , NOTE_E4, 16 , NOTE_A4, 16 ,
  
  NOTE_B4, 8 , REST, 16 , NOTE_E4, 16 , NOTE_GS4, 16 , NOTE_B4, 16 , // 19
  NOTE_C5, 8 , REST, 16 , NOTE_E4, 16 , NOTE_E5, 16 , NOTE_DS5, 16 ,
  NOTE_E5, 16 , NOTE_DS5, 16 , NOTE_E5, 16 , NOTE_B4, 16 , NOTE_D5, 16 , NOTE_C5, 16 ,
  NOTE_A4, 8 , REST, 16 , NOTE_C4, 16 , NOTE_E4, 16 , NOTE_A4, 16 ,
  NOTE_B4, 8 , REST, 16 , NOTE_E4, 16 , NOTE_C5, 16 , NOTE_B4, 16 ,
  NOTE_A4, 8 , REST, 16 , NOTE_B4, 16 , NOTE_C5, 16 , NOTE_D5, 16 , // 24 (1ère fin)
  
  // se répète à partir de 11
  NOTE_E5, - 8 , NOTE_G4, 16 , NOTE_F5, 16 , NOTE_E5, 16 ,
  NOTE_D5, - 8 , NOTE_F4, 16 , NOTE_E5, 16 , NOTE_D5, 16 , // 12
  
  NOTE_C5, - 8 , NOTE_E4, 16 , NOTE_D5, 16 , NOTE_C5, 16 , // 13
  NOTE_B4, 8 , REST, 16 , NOTE_E4, 16 , NOTE_E5, 16 , REST, 16 ,
  REST, 16 , NOTE_E5, 16 , NOTE_E6, 16 , REST, 16 , REST, 16 , NOTE_DS5, 16 ,
  NOTE_E5, 16 , REST, 16 , REST, 16 , NOTE_DS5, 16 , NOTE_E5, 16 , NOTE_DS5, 16 ,
  NOTE_E5, 16 , NOTE_DS5, 16 , NOTE_E5, 16 , NOTE_B4, 16 , NOTE_D5, 16 , NOTE_C5, 16 ,
  NOTE_A4, 8 , REST, 16 , NOTE_C4, 16 , NOTE_E4, 16 , NOTE_A4, 16 ,
  
  NOTE_B4, 8 , REST, 16 , NOTE_E4, 16 , NOTE_GS4, 16 , NOTE_B4, 16 , // 19
  NOTE_C5, 8 , REST, 16 , NOTE_E4, 16 , NOTE_E5, 16 , NOTE_DS5, 16 ,
  NOTE_E5, 16 , NOTE_DS5, 16 , NOTE_E5, 16 , NOTE_B4, 16 , NOTE_D5, 16 , NOTE_C5, 16 ,
  NOTE_A4, 8 , REST, 16 , NOTE_C4, 16 , NOTE_E4, 16 , NOTE_A4, 16 ,
  NOTE_B4, 8 , REST, 16 , NOTE_E4, 16 , NOTE_C5, 16 , NOTE_B4, 16 ,
  NOTE_A4, 8 , REST, 16 , NOTE_C5, 16 , NOTE_C5, 16 , NOTE_C5, 16 , // 25 - 2ème fin

  // continue du 26
  NOTE_C5, 4 , NOTE_F5, - 16 , NOTE_E5, 32 , // 26
  NOTE_E5, 8 , NOTE_D5, 8 , NOTE_AS5, - 16 , NOTE_A5, 32 ,
  NOTE_A5, 16 , NOTE_G5, 16 , NOTE_F5, 16 , NOTE_E5, 16 , NOTE_D5, 16 , NOTE_C5, 16 ,
  NOTE_AS4, 8 , NOTE_A4, 8 , NOTE_A4, 32 , NOTE_G4, 32 , NOTE_A4, 32 , NOTE_B4, 32 ,
  NOTE_C5, 4 , NOTE_D5, 16 , NOTE_DS5, 16 ,
  NOTE_E5, - 8 , NOTE_E5, 16 , NOTE_F5, 16 , NOTE_A4, 16 ,
  NOTE_C5, 4 , NOTE_D5, - 16 , NOTE_B4, 32 ,
  
  
  NOTE_C5, 32 , NOTE_G5, 32 , NOTE_G4, 32 , NOTE_G5, 32 , NOTE_A4, 32 , NOTE_G5, 32 , NOTE_B4, 32 , NOTE_G5, 32 , NOTE_C5, 32 , NOTE_G5, 32 , NOTE_D5, 32 , NOTE_G5, 32 , // 33
  NOTE_E5, 32 , NOTE_G5, 32 , NOTE_C6, 32 , NOTE_B5, 32 , NOTE_A5, 32 , NOTE_G5, 32 , NOTE_F5, 32 , NOTE_E5, 32 , NOTE_D5, 32 , NOTE_G5, 32 , NOTE_F5, 32 , NOTE_D5, 32 ,
  NOTE_C5, 32 , NOTE_G5, 32 , NOTE_G4, 32 , NOTE_G5, 32 , NOTE_A4, 32 , NOTE_G5, 32 , NOTE_B4, 32 , NOTE_G5, 32 , NOTE_C5, 32 , NOTE_G5, 32 , NOTE_D5, 32 , NOTE_G5, 32 ,

  NOTE_E5, 32 , NOTE_G5, 32 , NOTE_C6, 32 , NOTE_B5, 32 , NOTE_A5, 32 , NOTE_G5, 32 , NOTE_F5, 32 , NOTE_E5, 32 , NOTE_D5, 32 , NOTE_G5, 32 , NOTE_F5, 32 , NOTE_D5, 32 , // 36
  NOTE_E5, 32 , NOTE_F5, 32 , NOTE_E5, 32 , NOTE_DS5, 32 , NOTE_E5, 32 , NOTE_B4, 32 , NOTE_E5, 32 , NOTE_DS5, 32 , NOTE_E5, 32 , NOTE_B4, 32 , NOTE_E5, 32 , NOTE_DS5, 32 ,
  NOTE_E5, - 8 , NOTE_B4, 16 , NOTE_E5, 16 , NOTE_DS5, 16 ,
  NOTE_E5, - 8 , NOTE_B4, 16 , NOTE_E5, 16 , REST, 16 ,

  REST, 16 , NOTE_DS5, 16 , NOTE_E5, 16 , REST, 16 , REST, 16 , NOTE_DS5, 16 , // 40
  NOTE_E5, 16 , NOTE_DS5, 16 , NOTE_E5, 16 , NOTE_B4, 16 , NOTE_D5, 16 , NOTE_C5, 16 ,
  NOTE_A4, 8 , REST, 16 , NOTE_C4, 16 , NOTE_E4, 16 , NOTE_A4, 16 ,
  NOTE_B4, 8 , REST, 16 , NOTE_E4, 16 , NOTE_GS4, 16 , NOTE_B4, 16 ,
  NOTE_C5, 8 , REST, 16 , NOTE_E4, 16 , NOTE_E5, 16 , NOTE_DS5, 16 ,
  NOTE_E5, 16 , NOTE_DS5, 16 , NOTE_E5, 16 , NOTE_B4, 16 , NOTE_D5, 16 , NOTE_C5, 16 ,

  NOTE_A4, 8 , REST, 16 , NOTE_C4, 16 , NOTE_E4, 16 , NOTE_A4, 16 , // 46
  NOTE_B4, 8 , REST, 16 , NOTE_E4, 16 , NOTE_C5, 16 , NOTE_B4, 16 ,
  NOTE_A4, 8 , REST, 16 , NOTE_B4, 16 , NOTE_C5, 16 , NOTE_D5, 16 ,
  NOTE_E5, - 8 , NOTE_G4, 16 , NOTE_F5, 16 , NOTE_E5, 16 ,
  NOTE_D5, - 8 , NOTE_F4, 16 , NOTE_E5, 16 , NOTE_D5, 16 ,
  NOTE_C5, - 8 , NOTE_E4, 16 , NOTE_D5, 16 , NOTE_C5, 16 ,
  NOTE_B4, 8 , REST, 16 , NOTE_E4, 16 , NOTE_E5, 16 , REST, 16 ,
  REST, 16 , NOTE_E5, 16 , NOTE_E6, 16 , REST, 16 , REST, 16 , NOTE_DS5, 16 ,

  NOTE_E5, 16 , REST, 16 , REST, 16 , NOTE_DS5, 16 , NOTE_E5, 16 , NOTE_D5, 16 , // 54
  NOTE_E5, 16 , NOTE_DS5, 16 , NOTE_E5, 16 , NOTE_B4, 16 , NOTE_D5, 16 , NOTE_C5, 16 ,
  NOTE_A4, 8 , REST, 16 , NOTE_C4, 16 , NOTE_E4, 16 , NOTE_A4, 16 ,
  NOTE_B4, 8 , REST, 16 , NOTE_E4, 16 , NOTE_GS4, 16 , NOTE_B4, 16 ,
  NOTE_C5, 8 , REST, 16 , NOTE_E4, 16 , NOTE_E5, 16 , NOTE_DS5, 16 ,
  NOTE_E5, 16 , NOTE_DS5, 16 , NOTE_E5, 16 , NOTE_B4, 16 , NOTE_D5, 16 , NOTE_C5, 16 ,
  
  NOTE_A4, 8 , REST, 16 , NOTE_C4, 16 , NOTE_E4, 16 , NOTE_A4, 16 , // 60
  NOTE_B4, 8 , REST, 16 , NOTE_E4, 16 , NOTE_C5, 16 , NOTE_B4, 16 ,
  NOTE_A4, 8 , REST, 16 , REST, 16 , REST, 8 ,
  NOTE_CS5, - 4 ,
  NOTE_D5, 4 , NOTE_E5, 16 , NOTE_F5, 16 ,
  NOTE_F5, 4 , NOTE_F5, 8 ,
  NOTE_E5, - 4 ,
  NOTE_D5, 4 , NOTE_C5, 16 , NOTE_B4, 16 ,
  NOTE_A4, 4 , NOTE_A4, 8 ,
  NOTE_A4, 8 , NOTE_C5, 8 , NOTE_B4, 8 ,
  NOTE_A4, - 4 ,
  NOTE_CS5, - 4 ,

  NOTE_D5, 4 , NOTE_E5, 16 , NOTE_F5, 16 , // 72
  NOTE_F5, 4 , NOTE_F5, 8 ,
  NOTE_F5, - 4 ,
  NOTE_DS5, 4 , NOTE_D5, 16 , NOTE_C5, 16 ,
  NOTE_AS4, 4 , NOTE_A4, 8 ,
  NOTE_GS4, 4 , NOTE_G4, 8 ,
  NOTE_A4, - 4 ,
  NOTE_B4, 4 , REST, 8 ,
  NOTE_A3, - 32 , NOTE_C4, - 32 , NOTE_E4, - 32 , NOTE_A4, - 32 , NOTE_C5, - 32 , NOTE_E5, - 32 , NOTE_D5, - 32 , NOTE_C5, - 32 , NOTE_B4, - 32 ,

  NOTE_A4, - 32 , NOTE_C5, - 32 , NOTE_E5, - 32 , NOTE_A5, - 32 , NOTE_C6, - 32 , NOTE_E6, - 32 , NOTE_D6, - 32 , NOTE_C6, - 32 , NOTE_B5, - 32 , // 80
  NOTE_A4, - 32 , NOTE_C5, - 32 , NOTE_E5, - 32 , NOTE_A5, - 32 , NOTE_C6, - 32 , NOTE_E6, - 32 , NOTE_D6, - 32 , NOTE_C6, - 32 , NOTE_B5, - 32 ,
  NOTE_AS5, - 32 , NOTE_A5, - 32 , NOTE_GS5, - 32 , NOTE_G5, - 32 , NOTE_FS5, - 32 , NOTE_F5, - 32 , NOTE_E5, - 32 , NOTE_DS5, - 32 , NOTE_D5, - 32 ,

  NOTE_CS5, - 32 , NOTE_C5, - 32 , NOTE_B4, - 32 , NOTE_AS4, - 32 , NOTE_A4, - 32 , NOTE_GS4, - 32 , NOTE_G4, - 32 , NOTE_FS4, - 32 , NOTE_F4, - 32 , // 84
  NOTE_E4, 16 , NOTE_DS5, 16 , NOTE_E5, 16 , NOTE_B4, 16 , NOTE_D5, 16 , NOTE_C5, 16 ,
  NOTE_A4, - 8 , NOTE_C4, 16 , NOTE_E4, 16 , NOTE_A4, 16 ,
  NOTE_B4, - 8 , NOTE_E4, 16 , NOTE_GS4, 16 , NOTE_B4, 16 ,

  NOTE_C5, 8 , REST, 16 , NOTE_E4, 16 , NOTE_E5, 16 , NOTE_DS5, 16 , // 88
  NOTE_E5, 16 , NOTE_DS5, 16 , NOTE_E5, 16 , NOTE_B4, 16 , NOTE_D5, 16 , NOTE_C5, 16 ,
  NOTE_A4, - 8 , NOTE_C4, 16 , NOTE_E4, 16 , NOTE_A4, 16 ,
  NOTE_B4, - 8 , NOTE_E4, 16 , NOTE_C5, 16 , NOTE_B4, 16 ,
  NOTE_A4, - 8 , REST, - 8 ,
  REST, - 8 , NOTE_G4, 16 , NOTE_F5, 16 , NOTE_E5, 16 ,
  NOTE_D5, 4 , REST, 8 ,
  REST, - 8 , NOTE_E4, 16 , NOTE_D5, 16 , NOTE_C5, 16 ,
  
  NOTE_B4, - 8 , NOTE_E4, 16 , NOTE_E5, 8 , // 96
  NOTE_E5, 8 , NOTE_E6, - 8 , NOTE_DS5, 16 ,
  NOTE_E5, 16 , REST, 16 , REST, 16 , NOTE_DS5, 16 , NOTE_E5, 16 , NOTE_DS5, 16 ,
  NOTE_E5, 16 , NOTE_DS5, 16 , NOTE_E5, 16 , NOTE_B4, 16 , NOTE_D5, 16 , NOTE_C5, 16 ,
  NOTE_A4, - 8 , NOTE_C4, 16 , NOTE_E4, 16 , NOTE_A4, 16 ,
  NOTE_B4, - 8 , NOTE_E4, 16 , NOTE_GS4, 16 , NOTE_B4, 16 ,

  NOTE_C5, 8 , REST, 16 , NOTE_E4, 16 , NOTE_E5, 16 , NOTE_DS5, 16 , // 102
  NOTE_E5, 16 , NOTE_DS5, 16 , NOTE_E5, 16 , NOTE_B4, 16 , NOTE_D5, 16 , NOTE_C5, 16 ,
  NOTE_A4, - 8 , NOTE_C4, 16 , NOTE_E4, 16 , NOTE_A4, 16 ,
  NOTE_B4, - 8 , NOTE_E4, 16 , NOTE_C5, 16 , NOTE_B4, 16 ,
 
};

int melody4 [] = {

  // Basé sur l'arrangement sur https://www.flutetunes.com/tunes.php?id=169
  
  NOTE_AS4, - 2 , NOTE_F4, 8 , NOTE_F4, 8 , NOTE_AS4, 8 , // 1
  NOTE_GS4, 16 , NOTE_FS4, 16 , NOTE_GS4, - 2 ,
  NOTE_AS4, - 2 , NOTE_FS4, 8 , NOTE_FS4, 8 , NOTE_AS4, 8 ,
  NOTE_A4, 16 , NOTE_G4, 16 , NOTE_A4, - 2 ,


  NOTE_AS4, 4 , NOTE_F4, - 4 , NOTE_AS4, 8 , NOTE_AS4, 16 , NOTE_C5, 16 , NOTE_D5, 16 , NOTE_DS5, 16 , // 7
  NOTE_F5, 2 , NOTE_F5, 8 , NOTE_F5, 8 , NOTE_F5, 8 , NOTE_FS5, 16 , NOTE_GS5, 16 ,
  NOTE_AS5, - 2 , NOTE_AS5, 8 , NOTE_AS5, 8 , NOTE_GS5, 8 , NOTE_FS5, 16 ,
  NOTE_GS5, - 8 , NOTE_FS5, 16 , NOTE_F5, 2 , NOTE_F5, 4 ,

  NOTE_DS5, - 8 , NOTE_F5, 16 , NOTE_FS5, 2 , NOTE_F5, 8 , NOTE_DS5, 8 , // 11
  NOTE_CS5, - 8 , NOTE_DS5, 16 , NOTE_F5, 2 , NOTE_DS5, 8 , NOTE_CS5, 8 ,
  NOTE_C5, - 8 , NOTE_D5, 16 , NOTE_E5, 2 , NOTE_G5, 8 ,
  NOTE_F5, 16 , NOTE_F4, 16 , NOTE_F4, 16 , NOTE_F4, 16 , NOTE_F4, 16 , NOTE_F4, 16 , NOTE_F4, 16 , NOTE_F4, 16 , NOTE_F4, 8 , NOTE_F4, 16 , NOTE_F4, 8 ,

  NOTE_AS4, 4 , NOTE_F4, - 4 , NOTE_AS4, 8 , NOTE_AS4, 16 , NOTE_C5, 16 , NOTE_D5, 16 , NOTE_DS5, 16 , // 15
  NOTE_F5, 2 , NOTE_F5, 8 , NOTE_F5, 8 , NOTE_F5, 8 , NOTE_FS5, 16 , NOTE_GS5, 16 ,
  NOTE_AS5, - 2 , NOTE_CS6, 4 ,
  NOTE_C6, 4 , NOTE_A5, 2 , NOTE_F5, 4 ,
  NOTE_FS5, - 2 , NOTE_AS5, 4 ,
  NOTE_A5, 4 , NOTE_F5, 2 , NOTE_F5, 4 ,

  NOTE_FS5, - 2 , NOTE_AS5, 4 ,
  NOTE_A5, 4 , NOTE_F5, 2 , NOTE_D5, 4 ,
  NOTE_DS5, - 2 , NOTE_FS5, 4 ,
  NOTE_F5, 4 , NOTE_CS5, 2 , NOTE_AS4, 4 ,
  NOTE_C5, - 8 , NOTE_D5, 16 , NOTE_E5, 2 , NOTE_G5, 8 ,
  NOTE_F5, 16 , NOTE_F4, 16 , NOTE_F4, 16 , NOTE_F4, 16 , NOTE_F4, 16 , NOTE_F4, 16 , NOTE_F4, 16 , NOTE_F4, 16 , NOTE_F4, 8 , NOTE_F4, 16 , NOTE_F4, 8
  
};

int melody5 [] = {


  // Thème d'Hedwige dans les films Harry Potter
  // Socre de https://musescore.com/user/3811306/scores/4906610
  
  REST, 2 , NOTE_D4, 4 ,
  NOTE_G4, - 4 , NOTE_AS4, 8 , NOTE_A4, 4 ,
  NOTE_G4, 2 , NOTE_D5, 4 ,
  NOTE_C5, - 2 ,
  NOTE_A4, - 2 ,
  NOTE_G4, - 4 , NOTE_AS4, 8 , NOTE_A4, 4 ,
  NOTE_F4, 2 , NOTE_GS4, 4 ,
  NOTE_D4, - 1 ,
  NOTE_D4, 4 ,

  NOTE_G4, - 4 , NOTE_AS4, 8 , NOTE_A4, 4 , // 10
  NOTE_G4, 2 , NOTE_D5, 4 ,
  NOTE_F5, 2 , NOTE_E5, 4 ,
  NOTE_DS5, 2 , NOTE_B4, 4 ,
  NOTE_DS5, - 4 , NOTE_D5, 8 , NOTE_CS5, 4 ,
  NOTE_CS4, 2 , NOTE_B4, 4 ,
  NOTE_G4, - 1 ,
  NOTE_AS4, 4 ,
     
  NOTE_D5, 2 , NOTE_AS4, 4 , // 18
  NOTE_D5, 2 , NOTE_AS4, 4 ,
  NOTE_DS5, 2 , NOTE_D5, 4 ,
  NOTE_CS5, 2 , NOTE_A4, 4 ,
  NOTE_AS4, - 4 , NOTE_D5, 8 , NOTE_CS5, 4 ,
  NOTE_CS4, 2 , NOTE_D4, 4 ,
  NOTE_D5, - 1 ,
  REST, 4 , NOTE_AS4, 4 ,  

  NOTE_D5, 2 , NOTE_AS4, 4 , // 26
  NOTE_D5, 2 , NOTE_AS4, 4 ,
  NOTE_F5, 2 , NOTE_E5, 4 ,
  NOTE_DS5, 2 , NOTE_B4, 4 ,
  NOTE_DS5, - 4 , NOTE_D5, 8 , NOTE_CS5, 4 ,
  NOTE_CS4, 2 , NOTE_AS4, 4 ,
  NOTE_G4, - 1 ,
  
};


int notes1 = sizeof(melody1) / sizeof(melody1[0]) / 2;
int notes2 = sizeof(melody2) / sizeof(melody2[0]) / 2;
int notes3 = sizeof(melody3) / sizeof(melody3[0]) / 2;   
int notes4 = sizeof(melody3) / sizeof(melody3[0]) / 2;
int notes5 = sizeof(melody3) / sizeof(melody3[0]) / 2;
// sizeof gives the number of bytes, each int value is composed of two bytes (16 bits)
// there are two values per note (pitch and duration), so for each note there are four bytes


// this calculates the duration of a whole note in ms
int wholenote = (60000 * 4) / tempo;

int divider = 0, noteDuration = 0;
unsigned long start_time = 0;
unsigned long end_time = 0;


void setup() {
  pinMode(buttonApin, INPUT_PULLUP);  
  pinMode(buttonBpin, INPUT_PULLUP);
  pinMode(buttonCpin, INPUT_PULLUP); 
}

#define ON 1
#define OFF 2
#define chang 3
 
 
int which = ON;
int whichMelodie = 1;
int thisNote = -2;


void loop() {
  if (which == ON) {
    if (millis() >= end_time) {      
      if (whichMelodie == 1) {
        next_note1();
      } else if (whichMelodie == 2) {
        next_note2();
      } else if (whichMelodie == 3) {
        next_note3();
      } else if (whichMelodie == 4) {
        next_note4();
      } else if (whichMelodie == 5) {
        next_note5();
      }
    }
  } else {
    noTone(buzzer);  
  } 
   
  if (digitalRead(buttonApin) == LOW)
  {
    which = OFF;
    delay(10);
  }
  if (digitalRead(buttonBpin) == LOW)
  {
    which = ON;
    delay(10);
  }
  if (digitalRead(buttonCpin) == LOW)
  {
    noTone(buzzer);
    whichMelodie += 1; // passer musique suivante
    if (whichMelodie > 5) {
      whichMelodie = 1;
    }  
    thisNote = -2;
    delay(100);
  }
}


void next_note1() {
  // iterate over the notes of the melody.
  // Remember, the array is twice the number of notes (notes + durations)
  if (thisNote < notes1 * 2) {
    thisNote = thisNote + 2;
    
    // calculates the duration of each note
    divider = melody1[thisNote + 1];
    if (divider > 0) {
      // regular note, just proceed
      noteDuration = (wholenote) / divider;
    } else if (divider < 0) {
      // dotted notes are represented with negative durations!!
      noteDuration = (wholenote) / abs(divider);
      noteDuration *= 1.5; // increases the duration in half for dotted notes
    }
  
    // we only play the note for 90% of the duration, leaving 10% as a pause
    tone(buzzer, melody1[thisNote], noteDuration * 0.9);

    // Wait for the specief duration before playing the next note.
    end_time = millis() + noteDuration;
  }
}


void next_note2() {
  // iterate over the notes of the melody.
  // Remember, the array is twice the number of notes (notes + durations)
  if (thisNote < notes2 * 2) {
    thisNote = thisNote + 2;
    
    // calculates the duration of each note
    divider = melody2[thisNote + 1];
    if (divider > 0) {
      // regular note, just proceed
      noteDuration = (wholenote) / divider;
    } else if (divider < 0) {
      // dotted notes are represented with negative durations!!
      noteDuration = (wholenote) / abs(divider);
      noteDuration *= 1.5; // increases the duration in half for dotted notes
    }
    
    // we only play the note for 90% of the duration, leaving 10% as a pause
    tone(buzzer, melody2[thisNote], noteDuration * 0.9);

    // Wait for the specief duration before playing the next note.
    end_time = millis() + noteDuration;
  }
}

void next_note3() {
  // iterate over the notes of the melody.
  // Remember, the array is twice the number of notes (notes + durations)
  if (thisNote < notes3 * 2) {
    thisNote = thisNote + 2;
    
    // calculates the duration of each note
    divider = melody3[thisNote + 1];
    if (divider > 0) {
      // regular note, just proceed
      noteDuration = (wholenote) / divider;
    } else if (divider < 0) {
      // dotted notes are represented with negative durations!!
      noteDuration = (wholenote) / abs(divider);
      noteDuration *= 1.5; // increases the duration in half for dotted notes
    }
    
    // we only play the note for 90% of the duration, leaving 10% as a pause
    tone(buzzer, melody3[thisNote], noteDuration * 0.9);

    // Wait for the specief duration before playing the next note.
    end_time = millis() + noteDuration;
  }
}

void next_note4() {
  // iterate over the notes of the melody.
  // Remember, the array is twice the number of notes (notes + durations)
  if (thisNote < notes4 * 2) {
    thisNote = thisNote + 2;
    
    // calculates the duration of each note
    divider = melody4[thisNote + 1];
    if (divider > 0) {
      // regular note, just proceed
      noteDuration = (wholenote) / divider;
    } else if (divider < 0) {
      // dotted notes are represented with negative durations!!
      noteDuration = (wholenote) / abs(divider);
      noteDuration *= 1.5; // increases the duration in half for dotted notes
    }
    
    // we only play the note for 90% of the duration, leaving 10% as a pause
    tone(buzzer, melody4[thisNote], noteDuration * 0.9);

    // Wait for the specief duration before playing the next note.
    end_time = millis() + noteDuration;
  }
}

void next_note5() {
  // iterate over the notes of the melody.
  // Remember, the array is twice the number of notes (notes + durations)
  if (thisNote < notes5 * 2) {
    thisNote = thisNote + 2;
    
    // calculates the duration of each note
    divider = melody5[thisNote + 1];
    if (divider > 0) {
      // regular note, just proceed
      noteDuration = (wholenote) / divider;
    } else if (divider < 0) {
      // dotted notes are represented with negative durations!!
      noteDuration = (wholenote) / abs(divider);
      noteDuration *= 1.5; // increases the duration in half for dotted notes
    }
  
    // we only play the note for 90% of the duration, leaving 10% as a pause
    tone(buzzer, melody5[thisNote], noteDuration * 0.9);

    // Wait for the specief duration before playing the next note.
    end_time = millis() + noteDuration;
  }
}


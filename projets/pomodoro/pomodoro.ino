#include <Arduino.h>

#include "blink.hpp"
#include "countdown.hpp"
#include "4_digit_7_segments_display.hpp"
#include "led.hpp"
#include "Button.h"
#include "buzzer.hpp"

// == Pins ====================================================================

int digits[] = {4, 5, 6, 7};
display_4_digits_7_segments_with_shift_register display(8, 9, 10, digits);
Button button(2, PULLUP, 3000);

countdown travail_c(25 * 60);
countdown pause_c(5 * 60);
blink blinker(200);

led red(11);
led blue(12);

buzzer sound(3);

// ============================================================================

#define ATTENTE_TRAVAIL 1
#define TRAVAIL 2
#define SUSPEND_TRAVAIL 3
#define ATTENTE_PAUSE 4
#define PAUSE 5
#define SUSPEND_PAUSE 6

int etat = ATTENTE_TRAVAIL;

// ==Etats ====================================================================

int attente_travail()
{
  display.off();
  if (button.checkPress() == 1)
  {
    sound.off();
    red.off();
    blue.on();
    travail_c.reinit();
    travail_c.start();
    etat = TRAVAIL;
  }
}

int travail()
{
  display.minutes_secondes(travail_c.remaning());
  travail_c.update();
  if (travail_c.is_elapsed())
  {
    red.on();
    blue.off();
    sound.beep();
    etat = ATTENTE_PAUSE;
  }

  if (button.checkPress() == 1)
  {
    travail_c.pause();
    blinker.start();
    etat = SUSPEND_TRAVAIL;
  }
}

int suspend_travail()
{
  blinker.update();
  if (blinker.is_on())
  {
    display.minutes_secondes(travail_c.remaning());
  }
  else
  {
    display.off();
  }
  if (button.checkPress() == 1)
  {
    travail_c.start();
    etat = TRAVAIL;
  }
  if (button.checkPress() == -1)
  {
    travail_c.reinit();
    travail_c.start();
  }
}

int attente_pause()
{
  display.off();
  if (button.checkPress() == 1)
  {
    sound.off();
    red.off();
    blue.on();
    pause_c.reinit();
    pause_c.start();
    etat = PAUSE;
  }
}

int pause()
{
  display.minutes_secondes(pause_c.remaning());
  pause_c.update();
  if (pause_c.is_elapsed())
  {
    red.on();
    blue.off();
    sound.beep();
    etat = ATTENTE_TRAVAIL;
  }

  if (button.checkPress() == 1)
  {
    pause_c.pause();
    blinker.start();
    etat = SUSPEND_PAUSE;
  }
}

int suspend_pause()
{
  blinker.update();
  if (blinker.is_on())
  {
    display.minutes_secondes(pause_c.remaning());
  }
  else
  {
    display.off();
  }
  if (button.checkPress() == 1)
  {
    pause_c.start();
    etat = PAUSE;
  }
  if (button.checkPress() == -1)
  {
    pause_c.reinit();
    pause_c.start();
  }
}

void setup()
{
  display.setup();
  red.setup();
  blue.setup();
  sound.setup();

  etat = ATTENTE_TRAVAIL;
}

void loop()
{
  switch (etat)
  {
  case TRAVAIL:
    travail();
    break;

  case ATTENTE_TRAVAIL:
    attente_travail();
    break;

  case SUSPEND_TRAVAIL:
    suspend_travail();
    break;

  case PAUSE:
    pause();
    break;

  case ATTENTE_PAUSE:
    attente_pause();
    break;

  case SUSPEND_PAUSE:
    suspend_pause();
    break;
  }
}
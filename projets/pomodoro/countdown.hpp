#ifndef COUNTDOWN_H
#define COUNTDOWN_H

#include <stdbool.h>

class countdown
{
  uint32_t _initial;
  uint32_t _counter;
  uint64_t _next_second;
  bool _running;

public:
  countdown(unsigned int initial) : _initial(initial), _counter(initial), _next_second(0), _running(false) {}

  bool is_elapsed();
  void start();
  void pause();
  void reinit();
  void update();
  int remaning();
};

#endif

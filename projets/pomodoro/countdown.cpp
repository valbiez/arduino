#include <Arduino.h>
#include "countdown.hpp"

bool countdown::is_elapsed()
{
  return _counter == 0;
}

void countdown::start()
{
  _next_second = millis() + 1000;
  _running = true;
}

void countdown::pause()
{
  _running = false;
}

void countdown::reinit()
{
  _counter = _initial;
}

void countdown::update()
{
  if (_running)
  {
    unsigned long long current = millis();
    if (current >= _next_second)
    {
      _next_second += 1000;
      _counter--;
      if (_counter < 0)
      {
        _counter = 0;
        _running = false;
      }
    }
  }
}

int countdown::remaning()
{
  return _counter;
}

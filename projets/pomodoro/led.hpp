#ifndef LED_H
#define LED_H

#include <Arduino.h>

class led
{
    uint8_t _pin;

public:
    led(int pin) : _pin(pin) {}

    void setup()
    {
        pinMode(_pin, OUTPUT);
    }

    void on()
    {
        digitalWrite(_pin, HIGH);
    }

    void off()
    {
        digitalWrite(_pin, LOW);
    }
};

#endif
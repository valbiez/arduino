#include <Arduino.h>
#include "blink.hpp"

void blink::update()
{
    unsigned long long current = millis();
    if (current >= _next_second)
    {
        _next_second += _delay;
        _on = !_on;
    }
}

void blink::start()
{
    _next_second = millis() + 1000;
}

bool blink::is_on()
{
    return _on;
}

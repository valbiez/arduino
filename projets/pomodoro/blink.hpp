#ifndef BLINK_H
#define BLINK_H

class blink
{
    bool _on;
    unsigned long long _next_second;
    uint16_t _delay;

public:
    blink(unsigned int delay) : _on(false), _next_second(0), _delay(delay) {}
    void update();
    void start();
    bool is_on();
};

#endif
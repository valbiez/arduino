#ifndef BUZZER_H
#define BUZZER_H

#include <Arduino.h>

class buzzer
{
    uint8_t _pin;

public:
    buzzer(int pin) : _pin(pin) {}

    void setup()
    {
    }

    void beep(unsigned int frequency = 5000)
    {
        tone(_pin, frequency);
    }

    void off()
    {
        noTone(_pin);
    }
};

#endif
#include "4_digit_7_segments_display.hpp"

const unsigned char number_table[] PROGMEM =
    {0x3f, 0x06, 0x5b, 0x4f, 0x66, 0x6d, 0x7d, 0x07, 0x7f, 0x6f, 0x77, 0x7c, 0x39, 0x5e, 0x79, 0x71, 0x00};

void display_4_digits_7_segments_with_shift_register::display_digit(uint8_t digit, uint8_t value)
{
    for (int i = 0; i < 4; ++i)
    {
        digitalWrite(_digits[i], HIGH);
    }
    digitalWrite(_latch, LOW);
    shiftOut(_data, _clock, MSBFIRST, number_table[value]);
    digitalWrite(_latch, HIGH);
    digitalWrite(_digits[digit], HIGH);
}

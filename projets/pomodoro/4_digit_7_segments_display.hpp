#include <Arduino.h>

class display_4_digits_7_segments_with_shift_register
{
    uint8_t _latch;
    uint8_t _clock;
    uint8_t _data;
    uint8_t _digits[4];

public:
    display_4_digits_7_segments_with_shift_register(int clock, int latch, int data, int digits[4]) : _clock(clock), _latch(latch), _data(data), _digits()
    {
        for (int i = 0; i < 4; ++i)
        {
            _digits[i] = digits[i];
        }
    }

    void setup()
    {
        pinMode(_latch, OUTPUT);
        pinMode(_clock, OUTPUT);
        pinMode(_data, OUTPUT);
        for (int i = 0; i < 4; ++i)
        {
            pinMode(_digits[i], OUTPUT);
        }
    }

    void off()
    {
        for (int i = 0; i < 4; ++i)
        {
            display_digit(i, 16);
        }
    }

    void number(int value)
    {
        display_digit(3, value % 10);
        display_digit(2, (value / 10) % 10);
        display_digit(1, (value / 100) % 10);
        display_digit(0, (value / 1000) % 10);
    }

    void minutes_secondes(int seconds)
    {
        int in_minutes = seconds / 60;
        int in_seconds = seconds % 60;
        display_digit(3, in_seconds % 10);
        display_digit(2, (in_seconds / 10) % 10);
        display_digit(1, in_minutes % 10);
        display_digit(0, (in_minutes / 10) % 10);
    }

private:
    void display_digit(uint8_t digit, uint8_t value);
};

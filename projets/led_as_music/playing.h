#ifndef Playing_h
#define Playing_h

#include "sequences.h"


struct playing {
  int led;
  struct led_sequence* sequence;
  int current_position;
  int whole_duration;
  unsigned long end_time;
};


struct playing playing_create(int led, struct led_sequence* sequence);
boolean playing_play(struct playing* playing);
boolean playing_is_playing(struct playing *playing);


#endif

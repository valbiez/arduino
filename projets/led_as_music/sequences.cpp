#include "sequences.h"


struct led_sequence sequence_create(int* orders, int tempo) {
  return {
    orders,
    sizeof(orders) / sizeof(orders[0]),
    90,
  };
}

boolean is_in_sequence(struct led_sequence *sequence, int index) {
  return index < sequence->length - 2;
}


int advance_index(struct led_sequence *sequence, int index) {
  return index + 2;
}


int duration_for_index(struct led_sequence *sequence, int index, int whole_duration) {
    int divider = sequence->orders[index + 1];
    if (divider > 0) {
      return whole_duration / divider;
    } 
    return (whole_duration / abs(divider)) * 1.5;
}


int off_sequence[] = {
  LOW,2, 
};

struct led_sequence off = sequence_create(off_sequence, 90);


#ifndef Clignote_h
#define Clignote_h

#include "sequences.h"
#include <string.h>


int un_clignotement[] = {
  HIGH,16, LOW,16,
};
size_t taille_de_un = sizeof(un_clignotement);

struct led_sequence clignotement_create(int count, int tempo) {
  int* orders = (int*) malloc(taille_de_un * count);
  for(int index = 0; index < count; index++) {
    memcpy(orders + index * 4, &un_clignotement, taille_de_un);
  }
  
  return sequence_create(orders, tempo);
}

int clignote_sequence[] = {
  HIGH,16, LOW,16,
  HIGH,16, LOW,16,
  HIGH,16, LOW,16,
  HIGH,16, LOW,16,
  HIGH,16, LOW,16,
  HIGH,16, LOW,16,
};


struct led_sequence clignote = clignotement_create(6, 90);

#endif

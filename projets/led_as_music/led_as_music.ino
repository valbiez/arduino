#include "sequences.h"
#include "playing.h"
#include "clignote.h"

int buttonApin = 9;
int ledPin2 = 4;


int slow_sequence[] = {
  HIGH,2, LOW,2, 
  HIGH,2, LOW,2, 
  HIGH,2, LOW,2, 
  HIGH,2, LOW,2, 
  HIGH,2, LOW,2, 
  HIGH,2, LOW,2, 
};


struct led_sequence slow_clignote = sequence_create(slow_sequence, 90);



void setup() {
  pinMode(buttonApin, INPUT_PULLUP);  
  pinMode(ledPin2, OUTPUT);
  pinMode(5, OUTPUT);
  Serial.begin(9600);
  Serial.println("go");        
}

struct playing init_red() {
  return playing_create(4, &clignote);
}

struct playing red = init_red();
struct playing white = playing_create(5, &off);


void loop() {
  if(!playing_play(&red)) {
    red = init_red();
  }
  playing_play(&white);
  
  if (digitalRead(buttonApin) == LOW)
  {
    white = playing_create(5, &slow_clignote);
    delay(10);
  }
}








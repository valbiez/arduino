#include "playing.h"


struct playing playing_create(int led, struct led_sequence* sequence) {
  struct playing result = {
    led, sequence, -2, (60000 * 4) / sequence->tempo, 0
  };
  return result;
}

boolean playing_play(struct playing* playing) {
  if (millis() >= playing->end_time) {
    struct led_sequence* current = playing->sequence;
    
    if (is_in_sequence(current, playing->current_position)) {
      playing->current_position = advance_index(current, playing->current_position);
    
      digitalWrite(playing->led, current->orders[playing->current_position]);
  
      int duration = duration_for_index(current, playing->current_position, playing->whole_duration);
      playing->end_time = millis() + duration;
    }
  }

  return playing_is_playing(playing);
}

boolean playing_is_playing(struct playing *playing) {
  return is_in_sequence(playing->sequence, playing->current_position);
}


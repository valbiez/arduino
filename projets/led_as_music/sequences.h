#ifndef Sequence_h
#define Sequence_h

#include <Arduino.h>


struct led_sequence {
  int* orders; //array
  int length;
  int tempo;
};

struct led_sequence sequence_create(int* orders, int tempo);
boolean is_in_sequence(struct led_sequence *sequence, int index);
int advance_index(struct led_sequence *sequence, int index);
int duration_for_index(struct led_sequence *sequence, int index, int whole_duration);


extern struct led_sequence off;

#endif
